/*Creación de Base de datos*/
CREATE DATABASE IngeneoDB;
/*Creación de Tabla Books*/
USE [IngeneoDB]
GO
/****** Object:  Table [dbo].[Books]    Script Date: 4/02/2022 7:41:30 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Books](
	[Id] [int] NOT NULL,
	[Title] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[PageCount] [int] NOT NULL,
	[Excerpt] [nvarchar](max) NOT NULL,
	[PublishDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Books] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/*Creación de Tabla Authors*/
USE [IngeneoDB]
GO
/****** Object:  Table [dbo].[Authors]    Script Date: 4/02/2022 7:40:35 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Authors](
	[Id] [int] NOT NULL,
	[IdBook] [int] NOT NULL,
	[FirstName] [nvarchar](50) NOT NULL,
	[LastName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Authors] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Authors]  WITH CHECK ADD  CONSTRAINT [FK_Authors_Books] FOREIGN KEY([IdBook])
REFERENCES [dbo].[Books] ([Id])
GO

ALTER TABLE [dbo].[Authors] CHECK CONSTRAINT [FK_Authors_Books]
GO
